﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Basic graph data structure
/// </summary>
[System.Serializable]

public class Graph<T>
{
    private Dictionary<int, LinkedList<Node<T>>> nodes {get;set;}

    public Graph()
    {
        nodes = new Dictionary<int, LinkedList<Node<T>>>();
    }

    public int numberNodes
    {
        get { return nodes.Count - 1; }
    }
    
    public int NumberNeighboursNode(int nodeNum)
    {
        return nodes[nodeNum].Count;
    }

    //Get all neighbour nodes
    public Node<T>[] GetNodeNeighbours(int nodeNum)
    {
        Node<T>[] result = new Node<T>[NumberNeighboursNode(nodeNum)];
        nodes[nodeNum].CopyTo(result, 0);
        return result;
    }
    
    public bool ExistNode(Node<T> nodeNum)
    {
        return nodes.ContainsKey(nodeNum.index);
    }

    public void InsertNode(Node<T> nodeNum)
    {
        if(!ExistNode(nodeNum))
        {
            LinkedList<Node<T>> listNodes = new LinkedList<Node<T>>();
            this.nodes.Add(nodeNum.index, listNodes);
        }
    }

    public void InsertEdge(Node<T> nodeNum1, Node<T> nodeNum2)
    {
        if(!ExistNode(nodeNum1))
        {
            InsertNode(nodeNum1);
        }
        if(!ExistNode(nodeNum2))
        {
            InsertNode(nodeNum2);
        }
        nodes[nodeNum1.index].AddLast(nodeNum2);
    }
}

public class Node<T>
{
    public int index;
    public T value;

    public Node(int key, T val)
    {
        index = key;
        value = val;
    }

}