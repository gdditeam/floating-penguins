﻿using UnityEngine;
using System.Collections;
using System.IO;

public class InfiniteRunnerData : MonoBehaviour
{
    private bool initialized = false;
    public Graph<GameObject> platformParts;

    public readonly string fileName = "SeamlessPiecesGraph.txt";

    public void Start()
    {
        if (!initialized)
            initialized = true;
        else
            return;

        LoadPlatformData();
    }
    
    public void LoadPlatformData()
    {
         var sr = new StreamReader(Application.dataPath + "/" + fileName);
         string content = sr.ReadToEnd();
         platformParts = GetGraphFromFile(content);
    }

    public Graph<GameObject> GetGraphFromFile(string text)
    {
        var graphBuilder = new Graph<GameObject>();

        var lines = text.Split("\n"[0]);
        foreach(var line in lines)
        {
            var nodes = line.Split();

            var nodeNum1 = System.Int32.Parse(nodes[0]);
            var nodeNum2 = System.Int32.Parse(nodes[1]);
            graphBuilder.InsertEdge(new Node<GameObject>(nodeNum1,SceneManager.platformParts[nodeNum1]), new Node<GameObject>(nodeNum2, SceneManager.platformParts[nodeNum2]));

        }
    
        return graphBuilder;
    }
}