﻿using UnityEngine;
using System.Collections.Generic;

public static class MemoryPooling
{
    class MemoryPool
    {
        LinkedList<GameObject> inPool;
        GameObject prefab;

        public MemoryPool(GameObject obj)
        {
            this.prefab = obj;
            inPool = new LinkedList<GameObject>();
        }

        public GameObject Instantiate(Vector3 pos, Quaternion rot)
        {
            GameObject prfab;
            if (inPool.Count == 0)
            {
                prfab = (GameObject)GameObject.Instantiate(prefab, pos, rot);
                prfab.AddComponent<MemoryPoolMember>().mPool = this;
            }
            else
            {
                prfab = inPool.First.Value;
                inPool.RemoveFirst();

                if (prfab == null)
                {
                    return Instantiate(pos, rot);
                }
            }

            prfab.transform.position = pos;
            prfab.transform.rotation = rot;
            prfab.SetActive(true);
            return prfab;
        }

        public void Destroy(GameObject obj)
        {
            obj.SetActive(false);

            inPool.AddLast(obj);
        }
    }

    class MemoryPoolMember : MonoBehaviour
    {
        public MemoryPool mPool;
    }

    static Dictionary<GameObject, MemoryPool> pools;

    static public GameObject Instantiate(GameObject prefab, Vector3 pos, Quaternion rot)
    {
        if (pools == null)
        {
            pools = new Dictionary<GameObject, MemoryPool>();
        }

        if (prefab != null && pools.ContainsKey(prefab) == false)
        {
            pools.Add(prefab, new MemoryPool(prefab));
        }

        return pools[prefab].Instantiate(pos, rot);
    }
    
    static public void Destroy(GameObject obj)
    {
        var poolMember = obj.GetComponent<MemoryPoolMember>();
        if (poolMember == null)
        {
            GameObject.Destroy(obj);
        }
        else
        {
            poolMember.mPool.Destroy(obj);
        }
    }

}