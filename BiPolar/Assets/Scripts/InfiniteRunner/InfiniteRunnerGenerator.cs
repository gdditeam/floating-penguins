﻿using UnityEngine;
using System.Collections;

/// WORK IN PROGRESS
[RequireComponent(typeof(InfiniteRunnerData))]
public class InfiniteRunnerGenerator : MonoBehaviour
{
    Node<GameObject>[] possibleMatch;
    public InfiniteRunnerData data;

    public int currentPartIndex;
    public int nextPartIndex;

    public void Start()
    {
        data = this.GetComponent<InfiniteRunnerData>();
        currentPartIndex = 1;
    }
    
    //needs to be triggered
    public void GenerateNext()
    {
        //get the object you want to generate
        possibleMatch = data.platformParts.GetNodeNeighbours(currentPartIndex);
        nextPartIndex = Random.Range(0, possibleMatch.Length);
        GameObject nextPart = possibleMatch[nextPartIndex].value;
        //get the position where you have to generate
        Vector3 position = new Vector3(0, 0, 0);
        //generate using pool
        MemoryPooling.Instantiate(nextPart, position, Quaternion.identity);
    }





}
