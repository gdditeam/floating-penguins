﻿using UnityEngine;
using System.Collections;

public class PCInputProvider : AInputProvider
{
    private int axisResult;
    void Update()
    {
        axisResult = GetAxisResult("Horizontal");
        if (axisResult != 0)
        {
            AInputProvider.currentInput.direction = (InputContainer.Direction) axisResult;
        }
        axisResult = GetAxisResult("Jump");
        AInputProvider.currentInput.shouldFly = axisResult == 0 ? false : true;
    }

    void LateUpdate()
    {
        ResetCurrentInput();
    }

    int GetAxisResult(string axisName)
    {
        return (int)Input.GetAxis(axisName);
    }

    void ResetCurrentInput()
    {
        AInputProvider.currentInput.direction = InputContainer.Direction.none;
        AInputProvider.currentInput.shouldFly = false;
    }
}
