﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Base class for InputProvider.
/// Subclasses should be implemented for every platform.
/// Override Update to get input on every frame
/// </summary>

abstract public class AInputProvider : MonoBehaviour
{
    static public InputContainer currentInput;

    static protected InputContainer GetInput()
    {
        return currentInput;
    }

}
