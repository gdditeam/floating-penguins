﻿using UnityEngine;
using System.Collections;



public class InputContainer 
{

    public enum Direction 
    {
        left = -1,
        right = 1,
        up = 2,
        none = 0      
    }

    public bool shouldFly;
    public Direction direction;
    	
}
