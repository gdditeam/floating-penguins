﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public static class SceneManager
{
    public static GameObject[] platformParts;

    private static bool initialized = false;

    public static void Init()
    {
        if (!initialized)
            initialized = true;
        else
            return;

        platformParts = Resources.LoadAll<GameObject>("PlatformParts");
    }

    public static void Update()
    {
        Init();
    }

    internal static void Reset()
    {
        Resources.UnloadUnusedAssets();
    }
}
