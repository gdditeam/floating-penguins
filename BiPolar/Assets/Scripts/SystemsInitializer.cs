﻿using UnityEngine;
using System.Collections;


public class SystemsInitializer : MonoBehaviour
{
    public void Awake()
    {
        Application.targetFrameRate = 120;
    }

    public void Start()
    {
        SceneManager.Init();
    }

    public void Update()
    {
        SceneManager.Update();
    }
}